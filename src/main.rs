use std::env;
use std::process;

use rust_training::Config;

fn main() {
    println!(">>> Hello, main!");

    let args: Vec<String> = env::args().collect();
    // dbg!(&args);

    let config = Config::build(&args).unwrap_or_else(|err| {
        println!("ERROR! Problem parsing arguments: {err}");
        process::exit(1);
    });

    if let Err(e) = rust_training::run(&config) {
        // Because run returns () in the success case, we only care about detecting an error, so we don’t need unwrap_or_else to return the unwrapped value, which would only be ()
        println!("Error! Run failed: {e}");
        process::exit(1);
    }

    println!(">>> Bye!");
}
